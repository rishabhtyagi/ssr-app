const baseConfig = require('./webpack.config.base.js')
const merge = require('webpack-merge');

baseConfig.forEach((item, index, arr) => {
  arr[index] = merge(arr[index], {
    mode: "development"
  })
})
console.log(baseConfig)
module.exports = baseConfig
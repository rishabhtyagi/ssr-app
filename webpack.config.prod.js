const baseConfig = require('./webpack.config.base.js')
const merge = require('webpack-merge');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin')

baseConfig.forEach((item, index, arr) => {
  arr[index] = merge(arr[index], {
    mode: "production",
    optimization: {
      minimizer: [new UglifyJsPlugin()],
    },
    plugins: [
      new CompressionPlugin()
    ]
  })
})
console.log(baseConfig)
module.exports = baseConfig
import express from 'express'
import cors from 'cors'
import { renderToString } from "react-dom/server"
import App from '../shared/App'
import React from 'react'
import serialize from "serialize-javascript"
import { StaticRouter } from "react-router-dom"
import { renderStatic } from "@uifabric/merge-styles/lib-commonjs/server"
import { initializeIcons } from 'office-ui-fabric-react'
const fetch = require('node-fetch')

const app = express()

app.use(cors())

app.use(express.static("public"))

initializeIcons();

app.get("*", (req, res, next) => {
  const newLocal = `https://api.github.com/users`
  fetch(newLocal).then((response) => {
    return response.json();
  }).then((data) => {
    const { html, css } = renderStatic(() => renderToString(
      <StaticRouter location={req.url} context={data}>
        <App />
      </StaticRouter>
    ));
    res.send(`
        <!DOCTYPE html>
        <html>
          <head>
            <style>${css}</style>
            <title>SSR with RR</title>
            <script src="/bundle.js" defer></script>
            <script>window.__INITIAL_DATA__ = ${serialize(data)}</script>
          </head>
          <body>
            <div id="app">${html}</div>
          </body>
        </html>`
    )
  }).catch(err => console.log(`Error:${err}`))
})

app.listen(3001, () => {
  console.log(`Server is listening on port: 3001`)
})


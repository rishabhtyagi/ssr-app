import React from 'react'
import { Link } from 'react-router-dom'
import { DetailsList } from 'office-ui-fabric-react'

const FullRoster = (props) => {
   var GistsAPI = {}
   if (__isBrowser__) {
        GistsAPI = window.__INITIAL_DATA__
    } else {
        GistsAPI = props.staticContext
    }

    var columns = [
      {
        key: 'column1',
        name: 'Users',
        ariaLabel: 'Column operations for File type, Press to sort on File type',
        fieldName: 'login',
        minWidth: 140,
        maxWidth: 310,
        data: 'string',
        isPadded: true
      },
      {
        key: 'column2',
        name: 'Visit',
        fieldName: 'id',
        onRender: (item) => {
          return <Link to={`gists/${item.id}`}>{item.id}</Link>
        }
      }
    ]
return (
  <DetailsList
    items={GistsAPI}
    columns={columns}
   />
    // <div>
    //     <ul>
    //         {
    //             GistsAPI.map(
    //                 p => (
    //                     <li key={p.id}>
    //                      <Link to={`/gists/${p.id}`}>
    //                      {p.login}
    //                      </Link>   
    //                     </li>
    //                 )
    //             )
    //         }
    //     </ul>
    // </div>
)
         }
export default FullRoster;
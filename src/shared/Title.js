import React from 'react'
import { Text } from 'office-ui-fabric-react';

export default function Title (props) {
  return (
    <div>
      <Text variant={"xxLarge"}>GitHub Gist</Text>
    </div>
  )
}
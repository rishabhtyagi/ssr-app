 import React from 'react'
 import { Route } from 'react-router-dom'
 
 const RouteWithSubRoutes = ({key, path, exact, component}) => (
    <Route
    key={key}
    path={path}
    exact={exact}
    component={component}
    />
  )
  export default RouteWithSubRoutes
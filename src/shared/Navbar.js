import React from 'react'
import { Link } from 'react-router-dom'
import { Text } from 'office-ui-fabric-react';

export default function Navbar() {

  return (
    <header>
      <nav>
      <div><Link to='/'><Text>Home</Text></Link></div>
      <div><Link to='/gists'><Text>Gists List</Text></Link></div>
      </nav>
    </header>
  )
}


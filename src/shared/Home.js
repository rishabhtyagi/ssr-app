import React from 'react'
import { Text } from 'office-ui-fabric-react';

export default function Home () {
  return (
    <Text variant="xxLarge">Get a list of a users Gists</Text>
  )
}
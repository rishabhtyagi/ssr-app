import React from 'react'
import { Switch, Route } from 'react-router-dom'
import routes from './routes'
import RouteWithSubRoutes from './RouteWithSubRoutes'
import { Router } from 'express'

const Main = () => (
    <Switch>
        {routes.map((route) => (
            <RouteWithSubRoutes key={route.path} exact={route.exact} path={route.path} component={route.component} />
        ))}
    </Switch>
)

export default Main;    
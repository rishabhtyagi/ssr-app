import Home from './Home'
import Gists from './Gists'
import GistsList from './GistsList'
import GistDetails from './GistDetails'

const routes = [
  {
    path: '/',
    exact: true,
    component: Home,
  },
  {
    path: '/gists',
    exact: false,
    component: Gists,
    routes: [
      {
        path: '/gists',
        exact: true,
        component: GistsList
      },
      {
        path: '/gists/:id',
        exact: false,
        component: GistDetails
      }
    ]
  }
]

export default routes
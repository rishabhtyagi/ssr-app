import React, { Fragment } from 'react'
import { Link, useParams, Redirect, Switch } from 'react-router-dom'
import { useId, useBoolean } from '@uifabric/react-hooks';
import {
  getTheme,
  mergeStyleSets,
  FontWeights,
  DefaultButton,
  Modal,
  Text,
  IconButton,
} from 'office-ui-fabric-react';

import './GistDetails.scss'

// The Player looks up the player using the number parsed from
// the URL's pathname. If no player is found with the given
// number, then a "player not found" message is displayed
const Player = (props) => {
    let GistsAPI = {}
    if (__isBrowser__) {
        GistsAPI = window.__INITIAL_DATA__
    } else {
        GistsAPI = props.staticContext
    }
    let { id } = useParams();
    id = parseInt(id, 10)
    const gist = GistsAPI.find(x => x.id === id)
    if (!gist) {
        return (
            <Switch>
                <Redirect to='/gists'/>
            </Switch>
        )
    }

    const titleId = useId('title');
    const [isModalOpen, { setTrue: showModal, setFalse: hideModal }] = useBoolean(false);
    const [isDraggable, { toggle: toggleIsDraggable }] = useBoolean(false);
    const cancelIcon = { iconName: 'Cancel' };

    return (
        <Fragment>
          <div className="detail">
              <Text variant="xLarge">Gist Details</Text>
              <Text>"Login: "{gist.login}</Text>
              <Text>"ID :"{gist.id}</Text>
              <Text>"URL :"{gist.url}</Text>
              <Text>"Type :"{gist.type}</Text>
              <Link to='/gists'><Text>Back</Text></Link>
          </div>
          <DefaultButton onClick={showModal} text="Open Modal" />
          <Modal
            titleAriaId={titleId}
            isOpen={isModalOpen}
            onDismiss={hideModal}
            isBlocking={false}
            containerClassName={contentStyles.container}
            dragOptions={isDraggable ? dragOptions : undefined}
          >
            <div className={contentStyles.header}>
              <span id={titleId}>Lorem Ipsum</span>
              <IconButton
                styles={iconButtonStyles}
                iconProps={cancelIcon}
                ariaLabel="Close popup modal"
                onClick={hideModal}
              />
            </div>
            <div className={contentStyles.body}>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lorem nulla, malesuada ut sagittis sit
                amet, vulputate in leo. Maecenas vulputate congue sapien eu tincidunt. Etiam eu sem turpis. Fusce tempor
                sagittis nunc, ut interdum ipsum vestibulum non. Proin dolor elit, aliquam eget tincidunt non, vestibulum ut
                turpis. In hac habitasse platea dictumst. In a odio eget enim porttitor maximus. Aliquam nulla nibh,
                ullamcorper aliquam placerat eu, viverra et dui. Phasellus ex lectus, maximus in mollis ac, luctus vel eros.
                Vivamus ultrices, turpis sed malesuada gravida, eros ipsum venenatis elit, et volutpat eros dui et ante.
                Quisque ultricies mi nec leo ultricies mollis. Vivamus egestas volutpat lacinia. Quisque pharetra eleifend
                efficitur.
              </p>
              </div>
          </Modal>
        </Fragment>
    )
}

const theme = getTheme();
const contentStyles = mergeStyleSets({
  container: {
    display: 'flex',
    flexFlow: 'column nowrap',
    alignItems: 'stretch',
    maxWidth: 600
  },
  header: [
    // tslint:disable-next-line:deprecation
    theme.fonts.xLargePlus,
    {
      flex: '1 1 auto',
      borderTop: `4px solid ${theme.palette.themePrimary}`,
      color: theme.palette.neutralPrimary,
      display: 'flex',
      alignItems: 'center',
      fontWeight: FontWeights.semibold,
      padding: '12px 12px 14px 24px',
    },
  ],
  body: {
    flex: '4 4 auto',
    padding: '0 24px 24px 24px',
    overflowY: 'hidden',
    selectors: {
      p: { margin: '14px 0' },
      'p:first-child': { marginTop: 0 },
      'p:last-child': { marginBottom: 0 },
    },
  },
});

const iconButtonStyles = {
  root: {
    color: theme.palette.neutralPrimary,
    marginLeft: 'auto',
    marginTop: '4px',
    marginRight: '2px',
  },
  rootHovered: {
    color: theme.palette.neutralDark,
  },
};

export default Player

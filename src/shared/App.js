import React, { Component } from 'react'
import {
  Fabric,
  TextField,
  Stack,
} from 'office-ui-fabric-react';
import Navbar from './Navbar'
import Title from './Title'
import Main from './Main'

const stackTokens = { childrenGap: 50 };
const stackStyles = { root: { width: 650 } };
const columnProps = {
  tokens: { childrenGap: 15 },
  styles: { root: { width: 300 } },
};

class App extends Component {
  render() {
    return (
      <Fabric dir="ltr">
        <Title />
        <Navbar />
        <Main />
        <Stack horizontal tokens={stackTokens} styles={stackStyles}>
          <Stack {...columnProps}>
            <TextField label="Username" />
            <TextField // prettier-ignore
              label="Domain"
              prefix="https://"
              ariaLabel="Example text field with https:// prefix"
            />
          </Stack>
        </Stack>
      </Fabric>
    )
  }
}

export default App
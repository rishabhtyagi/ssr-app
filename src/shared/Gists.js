import React from 'react';
import { Switch } from "react-router-dom";
import RouteWithSubRoutes from './RouteWithSubRoutes'
import routes from './routes'

const Roster = () => {
  return (
    <main>
      <Switch>
        {routes[1].routes.map((route) => (
          <RouteWithSubRoutes key={route.path} exact={route.exact} path={route.path} component={route.component} />
        ))}
      </Switch>
    </main>
  )
}

export default Roster;

